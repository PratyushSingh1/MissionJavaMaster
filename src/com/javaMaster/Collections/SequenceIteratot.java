package com.javaMaster.Collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SequenceIteratot implements Iterator<Integer> {

	private Sequence sequence;
	private int nextValue;
	
	public SequenceIteratot(Sequence sequence)
	{
		this.sequence=sequence;
		this.nextValue=sequence.start;
	}
	@Override
	public boolean hasNext() {
		return this.nextValue<=this.sequence.limit;
	}

	@Override
	public Integer next() {
		if(this.sequence.limit<this.nextValue)
		{
			throw new NoSuchElementException("No Such Element");
		}
		int returnValue=this.nextValue;
		this.nextValue+=this.sequence.increment;
		return returnValue;
	}

}
