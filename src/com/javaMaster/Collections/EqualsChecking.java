package com.javaMaster.Collections;

import java.util.Arrays;
import java.util.List;

public class EqualsChecking {

	public static void main(String[] args) {

		Employee emp1=new Employee("pratyush", 22);
		Employee emp2=new Employee("ram", 26);
		Employee emp3=new Employee("krishna", 30);
		Employee emp4=new Employee("krishna", 30);
		List<Employee> empList=Arrays.asList(emp1,emp2,emp3);
		System.out.println(empList.contains(emp3));
		
		System.out.println(empList.contains(emp4));
	}
}
