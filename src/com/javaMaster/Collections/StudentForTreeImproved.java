package com.javaMaster.Collections;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class StudentForTreeImproved{

	private String name;
	private String address;
	private float grade;
	private static final 	Comparator<StudentForTreeImproved> nameComparator=new NameComparator();
	private static final 	Comparator<StudentForTreeImproved> addressComparator=new AddressComparator();
	
	
	public StudentForTreeImproved(String name, String address, float grade) {
		super();
		this.name = name;
		this.address = address;
		this.grade = grade;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public float getGrade() {
		return grade;
	}
	public void setGrade(float grade) {
		this.grade = grade;
	}
	@Override
	public String toString() {
		return "StudentForTree [name=" + name + ", address=" + address + ", grade=" + grade + "]";
	}
	
	@Override
	public boolean equals(Object object) {
		if(object instanceof StudentForTree)
		{
			StudentForTree student=(StudentForTree)object;
			if(student.getName().equals(this.getName()) && student.getAddress().equals(this.getAddress()))
			{
				return true;
			}else
				return false;
		}else
		return false;
	}
	@Override
	public int hashCode() {
		return this.getName().hashCode()+this.getAddress().hashCode();
	}
	public static void main(String[] args) {
		/**
		 * A way of using factory and singleton pattern to get the behavior of comparator 
		 * not a good singleton but kinda feel of it
		 */
		Set<StudentForTreeImproved> school=new TreeSet<>(StudentForTreeImproved.getAddressComparator());
		/**
		 * any object can be compared based on one thing and if the one thing is same then it will be skipped by set(If COMPARABLE)
		 * any object can be compared based on defined comparators and based on that if will be taken or skipped by set(COMPARATORS)
		 */
		school.add(new StudentForTreeImproved("pratyush","Hyderabad",5.5f));
		school.add(new StudentForTreeImproved("Vaibhav","Indore",6.5f));
		school.add(new StudentForTreeImproved("Saurabh","Kanpur",7.5f));
		school.add(new StudentForTreeImproved("pratyush","Varansi",4.5f));
		for(StudentForTreeImproved s:school)
		{
			System.out.println(s);
		}
	}
	private static Comparator<StudentForTreeImproved> getNameComparator()
	{
		return nameComparator;
	}
	private static Comparator<StudentForTreeImproved> getAddressComparator()
	{
		return addressComparator;
	}
	public static class NameComparator implements Comparator<StudentForTreeImproved>
	 {
		private NameComparator()
		{
			super();
		}
	 	@Override
	 	public int compare(StudentForTreeImproved o1, StudentForTreeImproved o2) {
	 		return o1.getName().compareTo(o2.getName());
	 	}
	 }
	 public static  class AddressComparator implements Comparator<StudentForTreeImproved>
	 	{
		 private AddressComparator()
		 {
			 super();
		 }
	 		@Override
	 		public int compare(StudentForTreeImproved o1, StudentForTreeImproved o2) {
	 			return o1.getAddress().compareTo(o2.getAddress());
	 		}
	 	}
}
