package com.javaMaster.Collections;

import java.util.HashSet;
import java.util.Set;

public class Items {

	private String name;
	private String description;
	
	public Items() {
		super();
	}

	public Items(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public static void main(String[] args) {

		Set<Items> hashSet=new HashSet<>();
		hashSet.add(new Items("pratyush","Book"));
		hashSet.add(new Items("Ramesh","Computer"));
		hashSet.add(new Items("Ramesh","Computer"));
		System.out.println(hashSet);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Items [name=" + name + ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
if(obj instanceof Items)
{
Items item=(Items)obj;	
	if(this.getName().equals(item.getName())&&this.getDescription().equals(item.getDescription()))
	{
		return true;
	}else return false;
}else
		return false;
	}

}
