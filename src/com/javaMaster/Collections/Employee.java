package com.javaMaster.Collections;

public class Employee {

	private String ename;
	private Integer age;
	public Employee(String ename, Integer age) {
		super();
		this.ename = ename;
		this.age = age;
	}
	@Override
	public String toString() {
		return "Employee [ename=" + ename + ", age=" + age + "]";
	}
	public boolean equals(Object obj) {
		if(obj instanceof Employee)
		{
			return this.age.equals(((Employee) obj).age) && this.ename.equals(((Employee) obj).ename);
		}
		else
		return false;
	}
}
