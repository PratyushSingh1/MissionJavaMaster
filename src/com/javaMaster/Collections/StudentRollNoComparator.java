package com.javaMaster.Collections;

import java.util.Comparator;

public class StudentRollNoComparator implements Comparator<StudentComparison> {

	@Override
	public int compare(StudentComparison o1, StudentComparison o2) {
		return o1.getRollNo().compareTo(o2.getRollNo());
	}

}
