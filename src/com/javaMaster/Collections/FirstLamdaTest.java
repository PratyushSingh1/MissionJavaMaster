package com.javaMaster.Collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class FirstLamdaTest {

	public static void main(String[] args) {

		List<Student> studentList=new ArrayList<Student>();
		studentList.addAll(Arrays.asList(new Student("pratyush", 0.03f),
																	new Student("ramesh", 5.5f),new Student("ram", 6.0f)));
		studentList.sort(new Comparator<Student>() {
			@Override
			public int compare(Student o1, Student o2) {
				return o1.getGrade().compareTo(o2.getGrade());
			}
		});
		System.out.println("Normal Sorting using anonymus inner class "+studentList);
		/**
		 * here comes the first lamda expression code written by great pratyush singh
		 * Since there is going to be only one method implementation so we can remove the students type too if wanted(o1,o2)
		 * it can be written is this way too
		 * studentList.sort((Student o1,Student o2)->o1.getGrade().compareTo(o2.getGrade()));
		 */
		studentList.sort((Student o1,Student o2)->{
			return o1.getGrade().compareTo(o2.getGrade());
		});
		System.out.println("sorting using lamda expression "+studentList);
	}

}
