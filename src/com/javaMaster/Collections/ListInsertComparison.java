package com.javaMaster.Collections;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ListInsertComparison {

	public static long insertMid(List<String> list,List<String> more)
	{
		int target=list.size()/2;
		long start=System.nanoTime();
		for(String addingString:more)
		{
			list.add(target++,addingString);
		}
		long end=System.nanoTime();
		return end-start;
	}
	
	public static long insertIter(List<String> list,List<String> more)
	{
		int target=list.size()/2;
		long start=System.nanoTime();
		ListIterator<String> li=list.listIterator(target);
		for(String addingString:more)
		{
			li.add(addingString);
		}
		long end=System.nanoTime();
		return end-start;
	}
	public static void main(String[] args) {

		final int COUNT=250_000;
		String[] items=new String[COUNT];
		for(int i=0;i<COUNT;i++)
		{
			items[i]=""+i;
		}
		List<String> list=Arrays.asList(items);
		LinkedList<String> ll=new LinkedList<String>();
		ll.addAll(list);
		System.out.println(insertMid(ll, list)/1_000_000_000.0);
		System.out.println("-------------------------------------------");
		System.out.println(insertIter(ll, list)/1_000_000_000.0);
		
	}
}
