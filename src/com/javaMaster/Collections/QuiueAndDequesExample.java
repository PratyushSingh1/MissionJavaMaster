package com.javaMaster.Collections;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.Queue;

/**
 * Created by PRATYUSH on 18-04-2018.
 */
public class QuiueAndDequesExample {
    public  static void main(String[] args)
    {
        Queue<String> queue=new ArrayDeque<>();
        Queue<String> dequeue=new ArrayDeque<>();
        queue.add("pratyush");
        queue.add("vaibhav");
        queue.add("hello");
        System.out.println(queue.element());
        System.out.println(queue);
        // Collections.asLifoQueue(queue);
        Deque<String> deque=new ArrayDeque<>();
        deque.push("one");
        deque.push("two");
        deque.push("three");
        deque.push("four");
        /*for(String s:deque)
        {
           System.out.println( deque.pop());
        }*/
        dequeue=Collections.asLifoQueue(deque);
        System.out.println(dequeue);
    }
}
