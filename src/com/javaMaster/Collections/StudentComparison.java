package com.javaMaster.Collections;

import java.util.ArrayList;
import java.util.List;

public class StudentComparison {

	private String name;
	private Integer grade;
	private String rollNo;
	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getGrade() {
		return grade;
	}


	public void setGrade(Integer grade) {
		this.grade = grade;
	}


	public String getRollNo() {
		return rollNo;
	}


	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}


	public static void main(String[] args) {

		List<StudentComparison> student=new ArrayList<>();
		StudentComparison studentComp=new StudentComparison();
		student.add(studentComp);
	}

}
