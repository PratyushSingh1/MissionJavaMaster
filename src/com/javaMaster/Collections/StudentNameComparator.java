package com.javaMaster.Collections;

import java.util.Comparator;

public class StudentNameComparator implements Comparator<StudentComparison> {

	@Override
	public int compare(StudentComparison student1, StudentComparison student2) {
		return student1.getName().compareTo(student2.getName());
	}

}
