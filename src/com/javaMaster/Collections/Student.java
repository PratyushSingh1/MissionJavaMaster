package com.javaMaster.Collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * 
 * @author PRATYUSH
 *Not a good approch as we are comaring same object with same objec twhich is not good,
 *So we need to define seperate comparators for each order that we want to place
 */
public class Student implements Comparable<Student> {

	private String name;
	private Float grade;
	
	
	public Student(String name, Float grade) {
		super();
		this.name = name;
		this.grade = grade;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getGrade() {
		return grade;
	}

	public void setGrade(Float grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "Student [name=" + getName() + ", grade=" + getGrade() + "]";
	}

	public static void main(String[] args) {

		Student student1=new Student("pratyush",2.2f);
		Student student2=new Student("Ram",1.2f);
		Student student3=new Student("Shyam",1.6f);
		List<Student> studentList=new ArrayList<>(Arrays.asList(student1,student2,student3));
		studentList.add(new Student("Sita",3.3f));
		studentList.add(new Student("Gita",0.3f));
		studentList.add(new Student("Krishna",5.3f));
		System.out.println("----------------------------------");
		for(Student student:studentList)
		{
			System.out.println(student);
		}
//can use new sort method of list with comparator as argument so that it can be compared		
		studentList.sort(new StudentGradeComparator());
	}

	@Override
	public int compareTo(Student student) {
		return Float.compare(this.grade, student.grade);
	}

}
