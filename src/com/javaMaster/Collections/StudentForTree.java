package com.javaMaster.Collections;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class StudentForTree implements Comparable<StudentForTree>{

	private String name;
	private String address;
	private float grade;
	public StudentForTree(String name, String address, float grade) {
		super();
		this.name = name;
		this.address = address;
		this.grade = grade;
	}
	public StudentForTree() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public float getGrade() {
		return grade;
	}
	public void setGrade(float grade) {
		this.grade = grade;
	}
	@Override
	public String toString() {
		return "StudentForTree [name=" + name + ", address=" + address + ", grade=" + grade + "]";
	}
	@Override
	public boolean equals(Object object) {
		if(object instanceof StudentForTree)
		{
			StudentForTree student=(StudentForTree)object;
			if(student.getName().equals(this.getName()) && student.getAddress().equals(this.getAddress()))
			{
				return true;
			}else
				return false;
		}else
		return false;
	}
	@Override
	public int hashCode() {
		return this.getName().hashCode()+this.getAddress().hashCode();
	}
	@Override
	public int compareTo(StudentForTree o) {
		return this.getName().compareTo(o.getName());
	}
	public static void main(String[] args) {
		/**
		 * if inner comperator class is not static the can be used in this way by creating object of outer class then inner class
		 */
		Set<StudentForTree> school=new TreeSet<>(new StudentForTree().new StudentForTreeAddressComparator());
		school.add(new StudentForTree("pratyush","Hyderabad",5.5f));
		school.add(new StudentForTree("Vaibhav","Indore",6.5f));
		school.add(new StudentForTree("Saurabh","Kanpur",7.5f));
		//
		school.add(new StudentForTree("pratyush","Varansi",4.5f));
		for(StudentForTree s:school)
		{
			System.out.println(s);
		}
	}
	public static class StudentForTreeNameComparator implements Comparator<StudentForTree>
	 {
	 	@Override
	 	public int compare(StudentForTree o1, StudentForTree o2) {
	 		return o1.getName().compareTo(o2.getName());
	 	}
	 }
	 public  class StudentForTreeAddressComparator implements Comparator<StudentForTree>
	 	{
	 		@Override
	 		public int compare(StudentForTree o1, StudentForTree o2) {
	 			return o1.getAddress().compareTo(o2.getAddress());
	 		}
	 	}
}
