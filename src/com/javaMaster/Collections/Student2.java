package com.javaMaster.Collections;


public class Student2 {

	private String firstName;

	public Student2(String firstName) {
		super();
		this.firstName = firstName;
	}

	public int hashCode() {
		return firstName.length();
	}
	@Override
	public String toString() {
		return "Student2 [firstName=" + firstName + "]";
	}
}
