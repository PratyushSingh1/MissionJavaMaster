package com.javaMaster.Collections;

import java.util.Iterator;
/**
 * 
 * @author PRATYUSH
 *this is best example for understanding core of functional programing
 *whenever the enhanced for loop runs the SerialiceIterator class methods are called where all the fun happens
 *so this class is iteratable!! 
 */
public class Sequence implements Iterable<Integer>{

	final int start,increment,limit;
	
	/**
	 * 
	 * @param start
	 * @param increment
	 * @param limit
	 */
	public Sequence(int start, int increment, int limit) {
		super();
		this.start = start;
		this.increment = increment;
		this.limit = limit;
	}
	public static void main(String[] args) {

		for(Integer i:new Sequence(1, 1, 100))
		{
			System.out.println(i);
		}
		/*Sequence s=new Sequence(1, 3, 13);
		Iterator<Integer> itr=s.iterator();
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}*/
	}
	@Override
	public Iterator<Integer> iterator() {
		return new SequenceIteratot(this);
	}
}
