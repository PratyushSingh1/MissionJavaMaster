package com.javaMaster.DataStructures;

import java.util.NavigableSet;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.PriorityBlockingQueue;

public class QueueTest {

	public static void main(String[] args) {
		NavigableSet<String> sortedSet=new TreeSet<>();
		sortedSet.add("Krishna");
		sortedSet.add("Ram");
		sortedSet.add("Angad");
		sortedSet.add("Hanuman");
		sortedSet.add("Lakshman");
		sortedSet.add("Sugrieve");
		sortedSet.add("Jamavant");
		System.out.println(sortedSet);
	}
}
