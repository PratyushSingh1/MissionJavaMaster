package com.javaMaster.DataStructures;

public class SinglyLinkedListCustom {

	private Node head;
	public int size=0;
	
	public void addFirst(int data) {
		Node node=new Node();
		node.setData(data);
		node.setNode(head);
		head=node;
		size++;
	}
	
	public void addLast(int data) {
		Node current=head;
		while(current.getNode()!=null) {
			current=current.getNode();
		}
		Node node=new Node();
		node.setData(data);
		current.setNode(node);
	}
	
	public Node deleteFirst() {
		Node deletedHead=head;
		head=head.getNode();
		size--;
		return deletedHead;
	}
	
	public Node deleteAfter(Node node) {
		Node current=head;
		Node deletedNode=null;
		while(current.getNode()!=null && current.getData() != node.getData()) {
			current=current.getNode();
		}
		if(current.getNode()!=null) {
			deletedNode=current.getNode();
			current.setNode(current.getNode().getNode());
		}
		size--;
		return deletedNode;
	}
	
	public void displaySinglyLinkedListCustom() {
		Node temp=head;
		while(temp!=null) {
			System.out.println("["+temp.getData()+"]");
			temp=temp.getNode();
		}
	}
	
	public Node findMiddleNode(Node head)
	{
		Node slowPointer, fastPointer; 
		slowPointer = fastPointer = head; 
 
		while(fastPointer !=null) { 
			fastPointer = fastPointer.getNode(); 
			if(fastPointer != null && fastPointer.getNode() != null) { 
				slowPointer = slowPointer.getNode(); 
				fastPointer = fastPointer.getNode(); 
			} 
		} 
		return slowPointer; 
	}
	
	public Node findnthElementFromLast(Node node,int elementCountFromLast) {
		Node tempNode=node;
		for(int i=1;i<(size-elementCountFromLast)+1;i++) {
			tempNode=tempNode.getNode();
		}
		return tempNode;
	}
	
	public static void main(String[] args) {
		
		SinglyLinkedListCustom custom=new SinglyLinkedListCustom();
		custom.addFirst(1);
		custom.addFirst(2);
		custom.addFirst(3);
		custom.addFirst(4);
		custom.addFirst(5);
		custom.addFirst(6);
		custom.displaySinglyLinkedListCustom();
		Node middle=custom.findMiddleNode(custom.head);
		System.out.println("Middle Element -->"+middle.getData());
		Node nthElementFromLast=custom.findnthElementFromLast(custom.head,6);
		System.out.println(6+" th element from last is --->"+nthElementFromLast.getData());
	}
	
}
