package com.javaMaster.StrategyDesignPattern;

import java.util.List;

public class BubbleSort implements SortingStrategy {

	@Override
	public List<Integer> sort(List<Integer> listToSort) {
		System.out.println("*** Implementing Bubble Sort ***");
		return listToSort;
	}

}
