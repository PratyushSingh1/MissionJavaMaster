package com.javaMaster.StrategyDesignPattern;

import java.util.ArrayList;
import java.util.List;

public class Employee {

	public void sortEmployees(SortingStrategy sortingStrategy) {
		List<Integer> listOfEmployees=new ArrayList<>();
		sortingStrategy.sort(listOfEmployees);
	}
	
	public static void main(String[] args) {
		Employee employee=new Employee();
		employee.sortEmployees(new SelectionSort());
	}
}
