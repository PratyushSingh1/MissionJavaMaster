package com.javaMaster.StrategyDesignPattern;

import java.util.List;

public class SelectionSort implements SortingStrategy {

	@Override
	public List<Integer> sort(List<Integer> listToSort) {
		System.out.println("*** Implementing Selection Sort ***");
		return listToSort;
	}

}
