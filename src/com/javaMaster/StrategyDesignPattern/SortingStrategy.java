package com.javaMaster.StrategyDesignPattern;

import java.util.List;

public interface SortingStrategy {

	List<Integer> sort(List<Integer> listToSort);
}
