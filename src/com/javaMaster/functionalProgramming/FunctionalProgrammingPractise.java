package com.javaMaster.functionalProgramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class FunctionalProgrammingPractise {

	public static List<String> courses =new ArrayList<>
	(Arrays.asList(new String[]{ "Spring", "Spring Boot", "API" , "Microservices", "AWS", 
		"PCF","Azure", "Docker", "Kubernetes" ,"Kubernetes" }));
	
	public static void main(String[] args) {
		printAllCoursesIndividually();
		System.out.println("======");
		printCoursesContainingWordSpring();
		System.out.println("======");
		printCoursesHavingNameAtLeastFourChar();
		System.out.println("======");
		createListWithLengthOfAllCourseTitle();
		System.out.println("======");
		printMaximumLengthStringInList();
		System.out.println("======");
		printListBasedOnSortedSizeOfCharacters();
	}
	
	public static void printAllCoursesIndividually(){
		courses.stream().forEach(System.out::println);
	}
	
	public static void printCoursesContainingWordSpring(){
		courses.stream().filter(x-> x.contains("Spring")).forEach(System.out::println);
	}

	public static void printCoursesHavingNameAtLeastFourChar(){
		courses.stream().filter(x-> x.length()>=4).forEach(System.out::println);
	}

	public static void createListWithLengthOfAllCourseTitle(){
		courses.stream().map(x->x.length()).collect(Collectors.toList())
		.forEach(System.out::println);
	}

	public static void printMaximumLengthStringInList(){
		System.out.println(courses.stream().max(Comparator.comparing(String::length)).get());
	}
	
	public static void printListBasedOnSortedSizeOfCharacters(){
		courses.stream().sorted(Comparator.comparing(String::length)).forEach(System.out::println);
	}
}
