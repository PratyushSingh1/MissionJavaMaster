package com.javaMaster.functionalProgramming;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by PRATYUSH on 26-04-2018.
 */
public class Car {
    private final int gasLevel;
    private final String colour;
    private List<String> passengers;
    private List<String> trunkContents;

    private Car(int gasLevel, String colour, List<String> passengers, List<String> trunkContents) {
        this.gasLevel = gasLevel;
        this.colour = colour;
        this.passengers = passengers;
        this.trunkContents = trunkContents;
    }

    public static Car withGasColourAndPassenger(int gas, String colour, String... passenger) {

        List<String> p = Collections.unmodifiableList(Arrays.asList(passenger));
        Car car = new Car(gas, colour, p, null);
        return car;
    }

    public static Car withGasColourPassengerAndtrunkConyents(int gas, String colour, String... passenger) {

        List<String> p = Collections.unmodifiableList(Arrays.asList(passenger));
        Car car = new Car(gas, colour, p, Arrays.asList("jack", "wrench", "spare wheel"));
        return car;
    }

    public int getGasLevel() {
        return gasLevel;
    }

    public String getColour() {
        return colour;
    }

    public List<String> getPassengers() {
        return passengers;
    }

    public List<String> getTrunkContents() {
        return trunkContents;
    }

    @Override
    public String toString() {
        return "Car{" +
                "gasLevel=" + gasLevel +
                ", colour='" + colour + '\'' +
                ", passengers=" + passengers +
                ", trunkContents=" + trunkContents +
                '}';
    }

    public static CarCriterion getCarColourCriterion() {
        return CAR_COLOUR_CRITERION;
    }
    /*changed Blocked Lamda To Expression Lamda*/
    private static final CarCriterion CAR_COLOUR_CRITERION = c -> c.colour.equals("Red");


   /* private static final CarCriterion CAR_COLOUR_CRITERION = new CarCriterion(){
        @Override
        public boolean test(Car c) {
            return c.colour.equals("Red");
        }
    };
   *//* private static final CarColourCriterion CAR_COLOUR_CRITERION = new CarColourCriterion();*/

    /*static class CarColourCriterion implements CarCriterion {

        @Override
        public boolean test(Car c) {
            return c.colour.equals("Red");
        }
    }*/

    public static CarCriterion getGasLevelCarCriterion(int threshold) {
        return new CarGasLevelCriterion(threshold);
    }

    private static class CarGasLevelCriterion implements CarCriterion {

        private Integer threshold;

        public CarGasLevelCriterion(Integer threshold) {
            this.threshold = threshold;
        }

        @Override
        public boolean test(Car c) {
            return c.gasLevel >= threshold;
        }
    }

    public static Comparator<Car> getGasComparator() {
        return gasComparator;
    }

    private static Comparator<Car> gasComparator =
            (Car o1, Car o2) -> o1.gasLevel - o2.gasLevel;

}
