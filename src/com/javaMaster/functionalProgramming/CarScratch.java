package com.javaMaster.functionalProgramming;

import java.util.*;

/**
 * Created by PRATYUSH on 26-04-2018.
 */

/**
 * Interface to define functional programming of which behavior of car is required as a seperate list
 */
interface CarCriterion{
    public  boolean test(Car c);
}
public class CarScratch {
    public static  void  main(String[] args)
    {
        List<Car> carList= Arrays.asList
                (Car.withGasColourAndPassenger(6,"Red","fred","Jim","Shila"),
                Car.withGasColourAndPassenger(10,"Octane","praty","ranjan","jalal"),
                Car.withGasColourAndPassenger(12,"Blue","vaibhav","saurabh","srinivas"),
                Car.withGasColourAndPassenger(15,"Red","laxman","ram")
                );
        showAll(carList);
        System.out.println("List of Car with red colour");
        showAll(getCarsByCriterion(carList, Car.getCarColourCriterion()));
        System.out.println("Get Sorted List of CarList");
        carList.sort(new CarComparator());
        showAll(carList);
        System.out.println("Get Cars by Passed Criterions");
        showAll(getCarsByCriterion(carList, Car.getGasLevelCarCriterion(10)));
        showAll(carList);
        carList.sort(Car.getGasComparator());
        System.out.println("Get Cars by After sorting based on gas level");
        showAll(carList);
    }

    private static void showAll(List<Car> carList) {
        for(Car car:carList)
            System.out.println(car);
        System.out.println("----------------------------------");
    }

    private  static List<Car> getCarsByCriterion(List<Car> carList,CarCriterion criterion)
    {
        List<Car> filterCarList=new ArrayList<>();
        for(Car c:carList)
        {
            if(criterion.test(c))
            {
                filterCarList.add(c);
            }
        }
        return filterCarList;
    }
}
class CarComparator implements Comparator<Car>
{
    @Override
    public int compare(Car o1, Car o2) {
        return o1.getPassengers().size()-o2.getPassengers().size();
    }
}
