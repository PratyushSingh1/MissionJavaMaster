package com.javaMaster.leetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SearchSuggestionsSystem {

	public static void main(String[] args) {
		
		SearchSuggestionsSystem suggestionsSystem=new SearchSuggestionsSystem();
		String[] products= {"mobile","mouse","moneypot","monitor","mousepad"};
		String searchWord = "mouse";
		List<List<String>> listOfSuggestions= suggestionsSystem
				.suggestedProducts(products, searchWord);
		listOfSuggestions.stream().forEach(System.out::println);
	}
    public List<List<String>> suggestedProducts(String[] products,
    		String searchWord) {
    	
    	List<List<String>> listOfSuggestions=new ArrayList<>();
    	List<String> listOfWords=new ArrayList<String>(Arrays.asList(products));
    	char[] ch= searchWord.toCharArray();
    	StringBuilder builder=new StringBuilder();
    	for(int i=0;i<ch.length;i++) {
    		builder.append(ch[i]);
    		ArrayList<String> suggestions= (ArrayList<String>) listOfWords.stream()
    					.filter(x->x.startsWith(builder.toString())).sorted()
    					.limit(3)
    					.collect(Collectors.toList());
    		listOfSuggestions.add(suggestions);
    	}
    	
		return listOfSuggestions;
    }
}
