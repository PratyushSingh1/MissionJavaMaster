package com.javaMaster.leetCode;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreeInOrderTraversal {

	private static List<Integer> list=new ArrayList<Integer>();
	public static void main(String[] args) {

		TreeNode tree = new TreeNode(1); 
		TreeNode treeRight= new TreeNode(2);
		TreeNode treeRightLeft= new TreeNode(3);
		treeRight.setLeft(treeRightLeft);
        tree.setRight(treeRight); 
        inorderTraversal(tree).forEach(System.out::print);;
	}
	
	public static List<Integer> inorderTraversal(TreeNode root) {
		inOrderTraversal(root);
		return list;
    }
	
	public static void inOrderTraversal(TreeNode node) {
		if(node==null) {
			return;
		}
		inorderTraversal(node.getLeft());
		list.add(node.getVal());
		System.out.println(node.getVal());
		inorderTraversal(node.getRight());
	}
	
	public static void inOrderTraversalWhile(TreeNode node) {
		
		while(node!=null) {
			
		}
	}
}
