package com.javaMaster.leetCode;

public class StringToInteger {

	public static void main(String[] args) {
		
	}
	
	public int myAtoi(String str) {
		str=str.replace(" ","");
		char[] ch= str.toCharArray();
		int number= getNumberSubStringFromString(ch);
		return 0;
    }

	private int getNumberSubStringFromString(char[] ch) {
		
		boolean isNegative=false;
		StringBuilder builder=new StringBuilder();
		int counter=0;
		if(ch[0]=='-') {
			isNegative=true;
			counter++;
		}
		while(Character.isDigit(ch[counter])) {
			builder.append(ch[counter]);
			counter++;
		}
		return 0;
	}
}
