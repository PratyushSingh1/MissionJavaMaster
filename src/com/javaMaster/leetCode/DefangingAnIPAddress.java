package com.javaMaster.leetCode;

public class DefangingAnIPAddress {

	public static void main(String[] args) {
		
		String ip="255.100.50.0";
		String defangedIpAddress= defangIPaddr(ip);
		System.out.println(defangedIpAddress);
	}
	public static String defangIPaddr(String address) {
		
		address=address.replace(".", "[.]");
		return address;
    }
}
