package com.javaMaster.leetCode;

public class HappyNumber {

	public static void main(String[] args) {
		System.out.println(isHappy(19));
	}
	 public static boolean isHappy(int n) {
		 
		 try {
			if(n==1) {
				 return true;
			 }
			 
			 int sum=0;
			 while(n!=0) {
				 int lastDigit=n%10;
				 sum=sum+(lastDigit*lastDigit);
				 n=n/10;
			 }
			return isHappy(sum);
		} catch (StackOverflowError e) {
			return false;
		}
	    }
}
