package com.javaMaster.leetCode;

import java.util.Arrays;

public class RotateArray {

	public static void main(String[] args) {
		int[] nums= {-1};
		rotate(nums, 2);
		System.out.println("Test");
	}
	public static void rotate(int[] nums, int k) {
        
		if(k > nums.length) 
	        k=k%nums.length;
		
		int[] newNums=new int[nums.length];
		int counter=0;
		int counter2=0;
		for(int i=nums.length-1;i>=0;i--) {
			if(k!=0) {
				newNums[counter]=nums[nums.length-k];
				counter++;
				k--;
			}else {
				newNums[counter]=nums[counter2];
				counter++;
				counter2++;
			}
		}
		for(int j=0;j<newNums.length;j++) {
			nums[j]=newNums[j];
		}
    }
}
