package com.javaMaster.leetCode;

public class PowerOfThree {

	public static void main(String[] args) {
		System.out.println(isPowerOfThree(27));
	}
	
	public static boolean isPowerOfThree(int n) {
		
		if(n<1) {
			return false;
		}
		while(n%3==0) {
			n=n/3;
		}
		if(n==1) {
			return true;
		}
		return false;
    }
}
