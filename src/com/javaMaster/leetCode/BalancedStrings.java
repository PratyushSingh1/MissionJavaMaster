package com.javaMaster.leetCode;

public class BalancedStrings {

	public static void main(String[] args) {
		
		String s="RLLLLRRRLR";
		int splittedParts= balancedStringSplit(s);
		System.out.println(splittedParts);
	}
	 public static int balancedStringSplit(String s) {
		 
		 char[] ch= s.toCharArray();
		 int lCount=0;
		 int RCount=0;
		 int splittedParts=0;
		 for(int i=0;i<ch.length;i++) {
			 if(ch[i]=='L') {
				 lCount++;
			 }else if(ch[i]=='R') {
				 RCount++;
			 }
			 if(lCount==RCount) {
				 splittedParts++;
				 lCount=0;
				 RCount=0;
			 }
		 }
		return splittedParts;
	  }
}
