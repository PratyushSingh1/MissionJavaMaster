package com.javaMaster.leetCode;

import java.util.ArrayList;
import java.util.List;

public class SymmetricTree {

	private static List<Integer> listOfInteger=new ArrayList<Integer>();
	
	public static void main(String[] args) {
	
		TreeNode root=new TreeNode(1);
		TreeNode lroot=new TreeNode(2);
		TreeNode rroot=new TreeNode(2);
		
		TreeNode llroot=new TreeNode(3);
		TreeNode rlroot=new TreeNode(4);
		
		TreeNode lrroot=new TreeNode(4);
		TreeNode rrroot=new TreeNode(3);
		
		lroot.setLeft(llroot);
		lroot.setRight(rlroot);
		
		rroot.setLeft(lrroot);
		rroot.setRight(rrroot);
		
		root.setLeft(lroot);
		root.setRight(rroot);
		System.out.println(isSymmetric(root));
	}
	
	 public static boolean isSymmetric(TreeNode root) {
		 if(root==null) {
			 return true;
		 }
		return isSymmetric(root.getLeft(),root.getRight());
	    }

	private static boolean isSymmetric(TreeNode left, TreeNode right) {
		if(left==null && right==null) {
			return true;
		}else if(left==null || right==null) {
			return false;
		}
		
		if(left.getVal()!=right.getVal()) {
			return false;
		}
		if(!isSymmetric(left.getRight(),right.getLeft())) {
			return false;
		}
		if(!isSymmetric(left.getLeft(),right.getRight())) {
			return false;
		}
		return true;
	}

}
