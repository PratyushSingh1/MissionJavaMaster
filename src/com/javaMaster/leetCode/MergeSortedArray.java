package com.javaMaster.leetCode;

import java.util.Arrays;

public class MergeSortedArray {
public static void main(String[] args) {
	
	int[] nums1=new int[] {1,2,3,0,0,0};
	int[] nums2=new int[] {2,5,6};
	merge(nums1, 3, nums2, 3);
}
public static void merge(int[] nums1, int m, int[] nums2, int n) {
    
	int counter=0;
	int counter2=0;
	while(counter<nums1.length) {
		
		if(counter>=m) {
			nums1[counter]=nums2[counter2];
			counter2++;
		}
		counter++;
	}
	Arrays.sort(nums1);
	
}
}
