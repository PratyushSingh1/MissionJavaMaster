package com.javaMaster.leetCode;

public class NumArray {

	private int[] nums= {}; 
	
	public NumArray(int[] nums) {
        this.nums=nums;
    }
    
    public int sumRange(int i, int j) {
    	
    	int counter=0;
    	int sum=0;
    	for(int a=i;a<=j;a++) {
    		sum+=nums[a];
    		counter++;
    	}
		return sum;
    }
    
    public static void main(String[] args) {
		int[] nums= {-2, 0, 3, -5, 2, -1};
		NumArray numArray=new NumArray(nums);
		System.out.println(numArray.sumRange(0, 5));
	}
}
