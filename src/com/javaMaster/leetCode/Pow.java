package com.javaMaster.leetCode;

public class Pow {

	public static void main(String[] args) {
		
		Pow pow=new Pow();
		double d= pow.myPow(2.00000, -2);
		System.out.println(d);
	}
	
	public double myPow(double base,int power) {
		
		double result=1;
		boolean isPowerNegative=Integer.signum(power)==-1?true:false;
		power=Math.abs(power);
		for(int i=0;i<power;i++) {
			result=result*base;
		}
		if(isPowerNegative) {
			result=1/result;
		}
		return result;
	}
}
