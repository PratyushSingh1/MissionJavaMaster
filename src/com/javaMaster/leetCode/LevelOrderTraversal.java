package com.javaMaster.leetCode;

import java.util.*;

public class LevelOrderTraversal {

	
	 private static List<Integer> list=new ArrayList<Integer>();

	public static void main(String[] args) {
	
		TreeNode root=new TreeNode(1);
		TreeNode lroot=new TreeNode(2);
		TreeNode rroot=new TreeNode(2);
		
		TreeNode llroot=new TreeNode(3);
		TreeNode rlroot=new TreeNode(4);
		
		TreeNode lrroot=new TreeNode(4);
		TreeNode rrroot=new TreeNode(3);
		
		lroot.setLeft(llroot);
		lroot.setRight(rlroot);
		
		rroot.setLeft(lrroot);
		rroot.setRight(rrroot);
		
		root.setLeft(lroot);
		root.setRight(rroot);
		levelOrderTraversalNew(root);
		System.out.println("===================================");
		inorderTraversal(root);
	}
	
	 private static List<Integer> levelOrderTraversalNew(TreeNode node) {
		 
		 Queue<TreeNode> queue=new LinkedList<TreeNode>();
		 queue.add(node);
		 while(!queue.isEmpty()) {
			 TreeNode treeNode= queue.poll();
			 list.add(treeNode.getVal());
			 System.out.print(treeNode.getVal());
			
			 if(treeNode.getRight()!=null) {
				 queue.add(treeNode.getRight());
			 }
			 if(treeNode.getLeft()!=null) {
				 queue.add(treeNode.getLeft());
			 }
		 }
		return list;
	 }
	 
	 private static void inorderTraversal(TreeNode node) {
		 
		 if(node==null) {
			 return;
		 }
		 inorderTraversal(node.getLeft());
		 System.out.print(node.getVal());
		 inorderTraversal(node.getRight());
	 }
}
