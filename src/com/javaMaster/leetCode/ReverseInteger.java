package com.javaMaster.leetCode;

public class ReverseInteger {

	public static void main(String[] args) {
		
		System.out.println(reverse(-121));
	}
	
	public static int reverse(int number) {
	    int rev = 0;
	    while(number != 0){
	    	int lastDigit=number%10;
	    	rev=(rev*10)+lastDigit;
	    	number=number/10;
	    }
	 
	    return rev;
	}
}
