package com.javaMaster.leetCode;

public class WildcardMatching {

	public static void main(String[] args) {
		
		WildcardMatching matching=new WildcardMatching();
		boolean flag= matching.isMatch("adceb","dc");
		System.out.println(flag);
	}
	
	public boolean isMatch(String string, String pattern) {
		
		boolean flag=false;
		if(!pattern.contains("?")&&!pattern.contains("*")) {
			flag= string.contentEquals(pattern);
		}else if(pattern.contains("*") && !pattern.contains("?")) {
			String[] ch= pattern.split("\\*");
			flag=isValidMatchForAsterik(string,ch);
		}else if(pattern.contains("?") && !pattern.contains("*")) {
			String[] ch= pattern.split("\\?");
			flag=isValidMatchForQuestion(string,ch);
		}
		
		return flag;
	}

	private boolean isValidMatchForQuestion(String string, String[] ch) {
		
		int currentCharacterIndex=0;
		for(int i=0;i<ch.length;i++) {
			int index= string.indexOf(ch[i]);
			if(index>=currentCharacterIndex) {
				currentCharacterIndex=index;
			}else {
				return false;
			}
		}
		return true;
	}

	private boolean isValidMatchForAsterik(String string, String[] ch) {
		
		int currentCharacterIndex=0;
		for(int i=0;i<ch.length;i++) {
			int index= string.indexOf(ch[i]);
			if(index>=currentCharacterIndex) {
				currentCharacterIndex=index;
			}else {
				return false;
			}
		}
		return true;
	}
}
