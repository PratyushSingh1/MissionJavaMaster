package com.javaMaster.leetCode;

public class ClimbStairs {

	
	public static void main(String[] args) {
		int waysToClimb=climbStairs(3);
		System.out.println(waysToClimb);
	}
	
	public static int climbStairs(int n) {
		return climb(0,n);
    	}

	private static int climb(int i, int n) {
		
		if(i>n) {
			return 0;
		}
		if(i==n) {
			return 1;
		}
		return climb(i+1, n)+climb(i+2, n);
	}

}
