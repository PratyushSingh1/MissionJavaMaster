package com.javaMaster.leetCode;

import java.util.List;

public class MergeTwoSortedList {

	public static void main(String[] args) {

		ListNode l1=new ListNode(1);
		ListNode ll1=new ListNode(2);
		ListNode lll1=new ListNode(4);
		
		ll1.next=lll1;
		l1.next=ll1;
		
		ListNode l2=new ListNode(1);
		ListNode ll2=new ListNode(3);
		ListNode lll2=new ListNode(4);
		
		ll2.next=lll2;
		l2.next=ll2;
		
		mergeTwoLists(l1, l2);
	}
	
	public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		
		ListNode temp1=l1;
		ListNode temp2=l2;
		ListNode combinedListNode=new ListNode(0);
		ListNode head=combinedListNode;
		while(temp1!=null || temp2!=null) {
			
			if(temp1!=null) {
				temp1=temp1.next;
				head.next=temp1;
			}
			
			else if(temp2!=null) {
				temp2=temp2.next;;
				head.next=temp2;
			}
			head=head.next;
		}
		
		return combinedListNode.next;
    }
}
