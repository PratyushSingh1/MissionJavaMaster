package com.javaMaster.leetCode;

public class SameTree {

	public static void main(String[] args) {
		TreeNode tree = new TreeNode(1); 
        tree.setLeft(new TreeNode(2)); 
        tree.setRight(new TreeNode(3)); 
        //tree.getLeft().setLeft(new TreeNode(4)); 
        //tree.getLeft().setRight(new TreeNode(5)); 

		TreeNode tree1 = new TreeNode(1); 
		tree1.setLeft(new TreeNode(2)); 
		tree1.setRight(new TreeNode(3)); 
		//tree1.getLeft().setLeft(new TreeNode(4)); 
		//tree1.getLeft().setRight(new TreeNode(5)); 

		boolean flag=isSameTree(tree, tree1);
		System.out.println(flag);
	}
	
	public static boolean isSameTree(TreeNode tree, TreeNode tree1) {
		return printInOrderTraversal(tree,tree1);
    }

	private static boolean printInOrderTraversal(TreeNode tree, TreeNode tree1) {

		if(tree==null && tree1==null) {
			return true;
		}
		if(tree==null || tree1==null) {
			return false;
		}
		if (tree.getVal() != tree1.getVal()) {
			return false;
		}
		return printInOrderTraversal(tree.getLeft(), tree1.getLeft())
				&& printInOrderTraversal(tree.getRight(), tree1.getRight());
	}
}
