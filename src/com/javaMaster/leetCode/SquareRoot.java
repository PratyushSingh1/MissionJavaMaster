package com.javaMaster.leetCode;

public class SquareRoot {

	
	public static int sqrt(int number) {
		
		int start=1;int result=1;
		
		while(result<=number) {
			start++;
			result=start*start;
		}
		
		return start-1;
	}
	
	public static void main(String[] args) {
		int squareRoot= sqrt(9);
		System.out.println("Square root is--->"+squareRoot);
	}

}
