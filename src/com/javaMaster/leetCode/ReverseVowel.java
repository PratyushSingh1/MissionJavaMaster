package com.javaMaster.leetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ReverseVowel {

	public static void main(String[] args) {

		String s="aA";
		String reversedString= reverseVowels(s);
		System.out.println(reversedString);
	}
	public static String reverseVowels(String s) {
		Character[] vowels=new Character[] {'a','e','i','o','u'};
		char[] ch= s.toCharArray();
		int j=0;
		StringBuilder sb=new StringBuilder();
		List<Character> vowelList=new ArrayList<Character>();
		for(int i=0;i<ch.length;i++) {
			if(Arrays.asList(vowels).contains(ch[i])) {
				vowelList.add(ch[i]);
			}
		}
		Collections.reverse(vowelList);
		for(int i=0;i<ch.length;i++) {
			if(Arrays.asList(vowels).contains(ch[i])) {
				ch[i]=vowelList.get(j);
				j++;
			}
			sb.append(ch[i]);
		}
		return sb.toString();
    }
}
