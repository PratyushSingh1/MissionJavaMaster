package com.javaMaster.leetCode;

import java.math.BigInteger;
import java.util.Arrays;

public class FactorialTrailingZeroes {

	private static BigInteger factorial=BigInteger.ONE;
	private static BigInteger[] array;
	
	public FactorialTrailingZeroes() {
		array=new BigInteger[10000];
		Arrays.fill(array,BigInteger.valueOf(-1));
	}
	
	public static void main(String[] args) {
		
		FactorialTrailingZeroes factorialTrailingZeroes=new FactorialTrailingZeroes();
		System.out.println(factorialTrailingZeroes.trailingZeroes(0));
	}
	 public int trailingZeroes(int n) {
		 
		 int counter=0;
		 BigInteger factorial=getFactorial(n);
		String factorialString= factorial.toString();
		char[] ch= factorialString.toCharArray();
		for(int i=ch.length-1;i>=0;i--) {
			if(ch[i]=='0') {
				counter++;
			}else {
				break;
			}
		}
		return counter;
	    }
	 
	private BigInteger getFactorial(int temp) {
		
		if(array[temp]!=BigInteger.valueOf(-1)) {
			return factorial.multiply(array[temp]);
		}else if(temp==0) {
			return  factorial.multiply(BigInteger.ONE);
		}else {
			factorial=factorial.multiply(BigInteger.valueOf(temp));
		}
		array[temp]=factorial;
		return getFactorial(temp-1);
	}
}
