package com.javaMaster.leetCode;

public class NumArrayAdvanced {

		private int[] nums= {};
		
	 	public NumArrayAdvanced(int[] nums) {
	    this.nums=nums;
	    }
	    
	    public void update(int i, int val) {
	    	this.nums[i]=val;
	    }
	    
	    public int sumRange(int i, int j) {
	    	
	    	int counter=0;
	    	int sum=0;
	    	for(int a=i;a<=j;a++) {
	    		sum+=nums[a];
	    		counter++;
	    	}
			return sum;
	    }
	    
	    public static void main(String[] args) {
	    		 int[] nums= {1, 3, 5};
	    		 NumArrayAdvanced obj = new NumArrayAdvanced(nums);
	    		 System.out.println(obj.sumRange(0, 2));
	    		 obj.update(1, 2);
	    		 System.out.println(obj.sumRange(0, 2));
		}
}
