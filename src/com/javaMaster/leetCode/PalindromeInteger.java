package com.javaMaster.leetCode;

public class PalindromeInteger {

	public static void main(String[] args) {
		System.out.println(isPalindrome(324));
	}
	
	public static int reverse(int number) {
		
		int rev=0;
		while(number!=0) {
			int lastDigit=number%10;
			rev=(rev*10)+lastDigit;
			number=number/10;
		}
		return rev;
	}
	
	 public static boolean isPalindrome(int x) {
		 
		 if(x<0) {
			 return false;
		 }
		 if(x==reverse(x)) {
			 return true;
		 }
		return false;
	   }
}
