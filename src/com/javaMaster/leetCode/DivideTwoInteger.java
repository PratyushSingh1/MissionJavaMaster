package com.javaMaster.leetCode;

public class DivideTwoInteger {

	public static void main(String[] args) {
	
		//System.out.println(newDivide(-10,2));
		System.out.println(newDivide(-2147483648,-3));
	}
	
	public static int divide(int dividend, int divisor) {
        int sum=0;
		int counter=0;
		boolean isNegative=false;
		if(dividend<0) {
			isNegative=true;
		}else if(divisor<0) {
			isNegative=true;
		}
		if(dividend<0 && divisor<0) {
			isNegative=false;
		}
		dividend=Math.abs(dividend);
		divisor=Math.abs(divisor);
		while(sum<=dividend){
			sum=sum+divisor;
			if(sum>dividend){
				return isNegative==true?0-counter:counter;
			}
			counter++;
		}
		return isNegative==true?0-counter:counter;
    }
	
	public static int newDivide(int dividend, int divisor) {
		
		
		int sign = ((dividend < 0) ^  (divisor < 0)) ? -1 : 1;
		
		 // dividend positive 
        long ldividend = Math.abs((long)dividend); 
        long  ldivisor = Math.abs((long)divisor); 
		
        
     // Initialize the quotient 
        int quotient = 0; 
          
        while (ldividend >= ldivisor) 
        { 
            ldividend -= ldivisor; 
            ++quotient; 
        } 
      
        return sign * quotient; 
	}
	
}
