package com.javaMaster.leetCode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class TenthLine {

	public static void main(String[] args) throws Exception {
		File file=new File("File.txt");
		BufferedReader bufferedReader=new BufferedReader(new FileReader(file));
		String line;
		int counter=0;
		while((line=bufferedReader.readLine())!=null) {
			if(counter==10) {
				System.out.println(line);
			}
			counter++;
		}
		bufferedReader.close();
	}
}
