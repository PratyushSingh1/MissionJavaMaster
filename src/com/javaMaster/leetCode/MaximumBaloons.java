package com.javaMaster.leetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaximumBaloons {

	public static void main(String[] args) {
		//String text="nlaebolko";
		//String text="loonbalxballpoon";
		String text="leetcode";
		int number= maxNumberOfBalloons(text);
		System.out.println(number);
	}
	public static int maxNumberOfBalloons(String text) {
		
		List<String> balloonList=new ArrayList<String>
		(Arrays.asList(new String[] {"b","a","l","l","o","o","n"}));
		int finalCount=0;
		while(!text.isEmpty()) {
			int counter=0;
			for(int i=0;i<balloonList.size();i++) {
				if(text.contains(balloonList.get(i))) {
					text=text.replaceFirst(balloonList.get(i), "");
					counter++;
				}
				if(i!=counter-1) {
					return finalCount;
				}
				if(counter==balloonList.size()) {
					finalCount++;
				}
			}
		}
		return finalCount;
    }
}
