package com.javaMaster.leetCode;

import java.util.ArrayList;
import java.util.List;

public class Permutations {

	public static void main(String[] args) {
		
		Permutations permutations=new Permutations();
		int[] nums=new int[]{1,2,3};
		List<List<Integer>> permutationList= permutations.permute(nums);
		System.out.println(permutationList);
	}
	
	 public List<List<Integer>> permute(int[] nums) {
		 
		 List<List<Integer>> list=new ArrayList<List<Integer>>();
		 
		 for(int i=0;i<nums.length;i++) {
			 for(int j=i+1;j<nums.length;j++) {
				 for(int k=j+1;k<nums.length;k++) {
					 List<Integer> sublist=new ArrayList<>();
					 sublist.add(nums[i]);
					 sublist.add(nums[j]);
					 sublist.add(nums[k]);
					 list.add(sublist);
				 }
			 }
		 }
		 
		return list;
	    }
}
