package com.javaMaster.Interfaces;

public class PayableInterfaceTest {

	public static void main(String[] args) {

		Payable[] payableArray=new Payable[4];
		payableArray[0]=new Invoice("01234", "seatInvoice", 2, 375.00);
		payableArray[1]=new Invoice("05678", "tireInvoice", 4, 876.00);
		payableArray[2]=new SalariedEmployee("pratyush","singh", "1-1-1-1-1",3000);
		payableArray[3]=new SalariedEmployee("Saurabh", "singh","123-123-123",400.00);
		for(Payable currentPayable:payableArray)
		{
			System.out.println("Current Payable object Is- "+currentPayable.toString()+" payamentAmountIs"
																		+currentPayable.getPaymentAmount());
		}
		
	}

}
