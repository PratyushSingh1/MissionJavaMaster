package com.javaMaster.Interfaces;

public interface Payable {

	double getPaymentAmount();
}
