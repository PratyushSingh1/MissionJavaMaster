package com.javaMaster.Interfaces;

public class SalariedEmployee extends Employee {

	private double weeklySalary;
	public SalariedEmployee(String firstName, String lastName, String socialSecurityNumber,double weeklySalary) {
		super(firstName, lastName, socialSecurityNumber);
		if(weeklySalary<0.0)
		{
			throw new IllegalArgumentException("weekly salary should be greater than 0.0s");
		}
		this.weeklySalary=weeklySalary;
	}

	
	public double getWeeklySalary() {
		return weeklySalary;
	}


	public void setWeeklySalary(double weeklySalary) {
		this.weeklySalary = weeklySalary;
	}


	@Override
	public double getPaymentAmount() {
		return getWeeklySalary();
	}

}
