package com.javaMaster.Interfaces;

public class Invoice implements Payable {


	private final String partNumber;
	private final String partDescription;
	private int quantity;
	private double pricePerItem;
	
	
	public Invoice(String partNumber, String partDescription, int quantity, double pricePerItem) {
		if(quantity<0)
		{
			throw new IllegalArgumentException("Quantity can't be less than 0");
		}
		if(pricePerItem<0.0)
		{
			throw new IllegalArgumentException("price per item can't be less than 0.0");
		}
		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.quantity = quantity;
		this.pricePerItem = pricePerItem;
	}

	public String getPartNumber() {
		return partNumber;
	}


	public String getPartDescription() {
		return partDescription;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		if(quantity<0)
		{
			throw new IllegalArgumentException("Quantity can't be less than 0");
		}
		this.quantity = quantity;
	}


	public double getPricePerItem() {
		if(pricePerItem<0.0)
		{
			throw new IllegalArgumentException("price per item can't be less than 0.0");
		}
		return pricePerItem;
	}


	public void setPricePerItem(double pricePerItem) {
		this.pricePerItem = pricePerItem;
	}


	@Override
	public double getPaymentAmount() {
		return getQuantity()*getPricePerItem();
	}

	@Override
	public String toString() {
		return "Invoice [partNumber=" + getPartNumber() + ", partDescription=" + getPartDescription() + ", quantity=" + getQuantity()
				+ ", pricePerItem=" + getPricePerItem() + "]";
	}

}
