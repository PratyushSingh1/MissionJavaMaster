package com.javaMaster.ExceptionHandling;
/**
 * 
 * @author PRATYUSH
 *The Example Shows how to get data from an exception object and iterate over it to get detals of exception that occured
 */
public class UsingChainedException {

	public static void main(String[] args) {


		try{
			method1();
		}catch(Exception exception)
		{
			System.err.println("===Print The Stack Trace===");
			exception.printStackTrace();
			System.err.println("===Get The Stack Trace===");
			StackTraceElement[] stackTraceElements=exception.getStackTrace();
			for(StackTraceElement traceElement:stackTraceElements)
			{
				System.out.println("Get Class Name:::"+traceElement.getClassName());
				System.out.println("Get File Name:::"+traceElement.getFileName());
				System.out.println("Get Line Number:::"+traceElement.getLineNumber());
				System.out.println("Get Method Name:::"+traceElement.getMethodName());
			}
		}
	}//end of main method
	public static void method1() throws Exception
	{
		method2();
	}
	public static void method2() throws Exception
	{
		method3();
	}
	public static void method3() throws Exception
	{
		throw new Exception("Exception thrown in method3");
	}
	
}
