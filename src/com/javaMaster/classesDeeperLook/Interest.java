package com.javaMaster.classesDeeperLook;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class Interest {

	public static void main(String[] args) {

		BigDecimal principal=BigDecimal.valueOf(1000.00);
		BigDecimal rate=BigDecimal.valueOf(0.005);
		
		System.out.println("Year "+" Amount on deposit");
		for(int year=1;year<=10;year++)
		{
			BigDecimal amount=principal.multiply(rate.add(BigDecimal.ONE).pow(year));
			System.out.println(NumberFormat.getCurrencyInstance().format(amount));
		}
	}

}
