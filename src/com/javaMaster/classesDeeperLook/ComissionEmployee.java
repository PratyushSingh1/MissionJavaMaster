package com.javaMaster.classesDeeperLook;

public class ComissionEmployee {

	private String firstName;
	private String lastName;
	private String socialSecurityNumber;
	private double grossSalary;
	private double commissionRate;
	public ComissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSalary,
			double commissionRate) {

		if(grossSalary<0.0)
		{
			throw new IllegalArgumentException("Gross salary cant be less than 0.0");
		}
		if(commissionRate<=0.0 || commissionRate>=1.0)
		{
			throw new IllegalArgumentException("Commission rate must be greater than 0 and less than 1");
		}
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
		this.grossSalary = grossSalary;
		this.commissionRate = commissionRate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}
	public double getGrossSalary() {
		return grossSalary;
	}
	public void setGrossSalary(double grossSalary) {
		if(grossSalary<0.0)
			throw new IllegalArgumentException("Gross salary cant be less than 0.0");
		this.grossSalary = grossSalary;
	}
	public double getCommissionRate() {
		return commissionRate;
	}
	public void setCommissionRate(double commissionRate) {
		if(commissionRate<=0.0 || commissionRate>=1.0)
			throw new IllegalArgumentException("Commission rate must be greater than 0 and less than 1");
		this.commissionRate = commissionRate;
	}
	public double earning()
	{
		return this.getGrossSalary()*this.getCommissionRate();
	}
	public String toString()
	{
		return "Comission Employee "+getFirstName()+" "+getLastName()+" "
		+"Social Security number  "+getSocialSecurityNumber()+" "
		+"gross salary "+getGrossSalary()+" Commission Rate"+getCommissionRate();
	}
	
}
