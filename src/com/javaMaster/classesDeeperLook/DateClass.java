package com.javaMaster.classesDeeperLook;

public class DateClass {

	private int month;
	private int day;
	private int year;
	private static final int[] dayPerMonth={0,31,28,31,30,31,30,31,31,30,31,30,31};
	
	public DateClass(int month,int day,int year)
	{
		if(month<=0||month>12)
			throw new IllegalArgumentException("month "+month+"must be between 1-12");
		if(day<=0||(day>dayPerMonth[month] && (month==2 && day==29)))
			throw new IllegalArgumentException("day"+day+"must be between 1-30");
		if(month==2 && day==29 && !(year % 400==0||(year%4==0 && year%100!=0)))
			throw new IllegalArgumentException("day out of range");
		
		this.month=month;
		this.day=day;
		this.year=year;
		System.out.printf("Date Object Constructor for date %s%n",this);
	}
	public String toString()
	{
		return String.format("%d/%d/%d", month,day,year);
	}
}
