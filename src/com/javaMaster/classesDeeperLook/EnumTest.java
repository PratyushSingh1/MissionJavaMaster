package com.javaMaster.classesDeeperLook;

import java.util.EnumSet;

public class EnumTest {

	public static void main(String[] args) {

		System.out.println("All Books");
		for(Book book:Book.values())
		{
			System.out.printf("%-10s%-45s%s%n",book,book.getBookName()
					,book.getCopyRightYear());
		}
		System.out.println("Books Between a specific range");
		for(Book book:EnumSet.range(Book.JHTP,Book.IW3HTP))
		{
			System.out.printf("%-10s%-45s%s%n",book,book.getBookName()
					,book.getCopyRightYear());
		}
	}

}
