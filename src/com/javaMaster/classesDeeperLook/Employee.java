package com.javaMaster.classesDeeperLook;


public class Employee {

	private String firstName;
	private String lastName;
	private DateClass birthDate;
	private DateClass hireDate;
	public Employee(String firstName, String lastName, DateClass birthDate,DateClass hireDate) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.hireDate = hireDate;
	}
	
	public String toString()
	{
		return String.format("%s,%s Hiredate:%s Birthday:%s",firstName,lastName,hireDate,birthDate);
	}
}
