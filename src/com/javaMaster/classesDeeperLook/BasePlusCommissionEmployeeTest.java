package com.javaMaster.classesDeeperLook;
/**
 * 
 * @author PRATYUSH
 *Runtime type of object that is refered to determines which method to be called at execution time.
 */
public class BasePlusCommissionEmployeeTest {

	public static void main(String[] args) {

		BasePlusCommission basePlusCommission=new BasePlusCommission("pratyush", "Singh", "12345", 10000.00, 0.5,20000.00);
		basePlusCommission.earning();
		System.out.println(basePlusCommission);
		ComissionEmployee commissionEmployee= new ComissionEmployee("ram","chandra","6789",15000, 0.6);
		System.out.println(commissionEmployee);
		System.out.println("==================================================");
		//polymorphic behaviour of commissionEmployee2 as even though commissionEmployee2 is a variable of superclass
		//variable but at runtime it refers to child class object so even though we r calling super class toString method
		//but it will return subclass to string method as at runtime the object is of subclass
		ComissionEmployee commissionEmployee2= basePlusCommission;
		System.out.println(commissionEmployee2.toString());

	}

}
