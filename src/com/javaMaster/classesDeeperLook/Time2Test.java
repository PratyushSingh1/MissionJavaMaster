package com.javaMaster.classesDeeperLook;

public class Time2Test {

	public static void main(String[] args) {

		Time2 t=new Time2();
		Time2 t1=new Time2(10);
		Time2 t2=new Time2(10,20);
		Time2 t3=new Time2(10,20,30);
		Time2 copyTime=new Time2(t3);
		
		displayTime("default",t);
		displayTime("hourly",t1);
		displayTime("hourly and minutes",t2 );
		displayTime("hourly minutes seconds", t3);
		displayTime("copy constructor",copyTime);
	}

	private static void displayTime(String header, Time2 time) {

		System.out.println(header+" "+time.toUniversalString()+" "+time.toString());
	}

}
