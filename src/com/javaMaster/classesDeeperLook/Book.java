package com.javaMaster.classesDeeperLook;

public enum Book {

	JHTP("Java How To Program","2015"),
	CHTP("C How To Program","2013"),
	IW3HTP("Internat and World Wide Web how to Program","2012"),
	CPPHTP("C++ How To Program","2014");
	
	private final String bookName;
	private final String copyRightYear;
	private Book(String bookName,String copyRightYear)
	{
		this.bookName=bookName;
		this.copyRightYear=copyRightYear;
	}
	public String getBookName() {
		return bookName;
	}
	public String getCopyRightYear() {
		return copyRightYear;
	}
}
