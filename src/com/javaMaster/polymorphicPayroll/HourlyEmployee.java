package com.javaMaster.polymorphicPayroll;

public class HourlyEmployee extends Employee {

	private double wages;
	private double hours;
	public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber,double wages,double hours) {
		super(firstName, lastName, socialSecurityNumber);
		if(wages<0.0)
		{
			throw new IllegalArgumentException("hourly salary can't be less than 0.0");
		}
		if(hours<0.0||hours>168)
		{
			throw new IllegalArgumentException("worked hours should be between 0-168");
		}
		this.hours=hours;
		this.wages=wages;
	}

	public double getWages() {
		return wages;
	}

	public void setWages(double wages) {
		this.wages = wages;
	}

	public double getHours() {
		return hours;
	}

	public void setHours(double hours) {
		this.hours = hours;
	}

	@Override
	public double earning() {
		if(getHours()<=40)
		{
			return getHours()*getWages();
		}
		else
		return 40*getWages()+(getHours()-40)*getWages()*1.5;
	}

	@Override
	public String toString() {
		return "HourlyEmployee ["+super.toString()+"wages=" + getWages() + ", hours=" +getHours() + "]";
	}

}
