package com.javaMaster.polymorphicPayroll;

public class SalariedEmployee extends Employee {

	private double weeklySalary;
	public SalariedEmployee(String firstName, String lastName, String socialSecurityNumber,double weeklySalary) {
		super(firstName, lastName, socialSecurityNumber);

		if(weeklySalary<0.0)
		{
			throw new IllegalArgumentException("salary cant be less than 0,0");
		}
		this.weeklySalary=weeklySalary;
	}
	
	public double getWeeklySalary() {
		return weeklySalary;
	}

	public void setWeeklySalary(double weeklySalary) {
		this.weeklySalary = weeklySalary;
	}

	@Override
	public double earning() {
		return getWeeklySalary();
	}

	@Override
	public String toString() {
		return "SalariedEmployee ["+ super.toString()+" weeklySalary=" +getWeeklySalary() + "]";
	}

}
