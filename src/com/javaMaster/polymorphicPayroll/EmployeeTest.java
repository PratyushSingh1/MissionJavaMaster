package com.javaMaster.polymorphicPayroll;

public class EmployeeTest {

	public static void main(String[] args) {

		SalariedEmployee salariedEmployee=new SalariedEmployee("pratyush", "singh","12345",20.2);
		HourlyEmployee hourlyEmployee=new HourlyEmployee("Saurabh", " singh","6789",10.0, 60);
		CommissionEmployee commissionEmployee=new CommissionEmployee("vaibhav","singh", "35678",32.0, 0.5);
		BasePlusCommission basePlusCommission=new BasePlusCommission("dashie","games", "234234",1200,0.5,15000);
		System.out.println("===================================================================");
		System.out.println("salaried Employee Earning"+salariedEmployee.earning());
		System.out.println("hourly Employee Earning"+hourlyEmployee.earning());
		System.out.println("commission Employee Earning"+commissionEmployee.earning());
		System.out.println("Base Plus Commission"+basePlusCommission.earning());
		Employee[] employee=new Employee[4];
		employee[0]=salariedEmployee;
		employee[1]=hourlyEmployee;
		employee[2]=commissionEmployee;
		employee[3]=basePlusCommission;
		System.out.println("Employee processed polymorphically");
		
		for(Employee emp:employee)
		{
			System.out.println(emp);
			if(emp instanceof BasePlusCommission)
			{
				BasePlusCommission basePlusCommission2=(BasePlusCommission) emp;
				basePlusCommission2.setBaseSalary(1.10*basePlusCommission2.getBaseSalary());
				System.out.println("New Base Salary with increase is"+basePlusCommission2.getBaseSalary());
			}
			System.out.println(emp.earning());
		}
	}

}
