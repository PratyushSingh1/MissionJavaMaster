package com.javaMaster.polymorphicPayroll;

public class BasePlusCommission extends CommissionEmployee{
	
	private double baseSalary;
	
	public BasePlusCommission(String firstName, String lastName, String socialSecurityNumber,double grossSales,double commission,double baseSalary) {
		super(firstName, lastName, socialSecurityNumber,grossSales,commission);
		if(baseSalary<0.0)
			throw new IllegalArgumentException("Base salary must be greater than 0.0");
		this.baseSalary=baseSalary;
	
	}
	
	public double getBaseSalary() {
		return baseSalary;
	}
	
	public void setBaseSalary(double baseSalary) {
		if(baseSalary<0.0)
			throw new IllegalArgumentException("Base salary must be greater than 0.0");
		this.baseSalary=baseSalary;	
		}
	public double earning()
	{
		return this.getBaseSalary()+super.earning();
	}
	public String toString()
	{
		return super.toString()+" Base Salary"+getBaseSalary();
	}
	
}

