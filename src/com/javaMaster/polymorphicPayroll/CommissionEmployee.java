package com.javaMaster.polymorphicPayroll;

public class CommissionEmployee extends Employee {

	private double grossSales;
	private double commissionRate;

	public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales, double commissionRate) {
		super(firstName, lastName, socialSecurityNumber);
	
		if(grossSales<0.0)
		{
			throw new IllegalArgumentException("sale cant be less than 0.0");
		}
		if(commissionRate<=0.0 || commissionRate>=1.0)
		{
			throw new IllegalArgumentException("comission rate must be greater than o.o and less than 1.0");
		}
		this.grossSales=grossSales;
		this.commissionRate=commissionRate;
	}
	
	public double getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(double grossSales) {

		if(grossSales<0.0)
		{
			throw new IllegalArgumentException("sale cant be less than 0.0");
		}

		this.grossSales = grossSales;
	}

	public double getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(double commissionRate) {

		if(commissionRate<=0.0 || commissionRate>=1.0)
		{
			throw new IllegalArgumentException("comission rate must be greater than o.o and less than 1.0");
		}
		this.commissionRate = commissionRate;
	}

	public double earning() {
		return getCommissionRate()*getGrossSales();
	}

	@Override
	public String toString() {
		return "CommissionEmployee ["+super.toString()+"grossSales=" + getGrossSales() + ", commissionRate=" + getCommissionRate() + "]";
	}
}
