package com.javaMaster.basic;

public class LogicTest {
/*
 package com.sutisoft.analytics.action.adhoc.helper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.opensymphony.xwork2.ActionSupport;
import com.sutisoft.hvm.hibernate.HibernateSessionFactory;
public class DynamicQueryTreeHelper extends ActionSupport {

	private static final Logger LOGGER = Logger.getLogger(DynamicQueryHelper.class.getName());
	Session session = HibernateSessionFactory.getSession();
	private static String FETCH_MODULE_QUERY="select qb_report_category.category_name from qb_report_category where "
												+ "qb_report_category.category_id is null";

	private static String FETCH_SUB_MODULE_QUERY="select qb_report_category.id as 'Child Id',qb_report_category.category_name as 'Child',ref.Parent as 'Parent'\r\n" + 
			"from\r\n" + 
			"(select qb_report_category.category_name as 'Parent',qb_report_category.id as 'id'\r\n" + 
			"from qb_report_category where qb_report_category.category_id is null)ref\r\n" + 
			"inner join qb_report_category on ref.id=qb_report_category.category_id; \r\n" + 
			"";
	public JSONArray getModulesToDisplayAsFolder(Integer unitId) {

		LOGGER.info("Entered into getModulesToDisplayAsFolder() in DynamicQueryTreeHelper.java");
		JSONArray jsonArrayOfParentFolders = new JSONArray();
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			conn = session.connection();
			statement = conn.createStatement();

			// get jsonArray by passing the result set
			rs = statement.executeQuery(FETCH_MODULE_QUERY);
			jsonArrayOfParentFolders = getJsonArray(rs);
		} catch (Exception exception) {
			LOGGER.error(this.getClass().getName() + " --> " + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " --> Error is : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOGGER.info("Exited from getModulesToDisplayAsFolder() in DynamicQueryTreeHelper.java");
		return jsonArrayOfParentFolders;
	}
	public JSONArray getSubModulesForParentFolder(Integer unitId) {

		LOGGER.info("Entered into getSubModulesForParentFolder() in DynamicQueryTreeHelper.java");
		JSONArray jsonArrayOfSubFolders = new JSONArray();
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Integer> listOfCategoryId=new ArrayList<>();
		try {
			conn = session.connection();
			statement = conn.createStatement();
			rs = statement.executeQuery(FETCH_SUB_MODULE_QUERY);
			while(rs.next())
			{
				listOfCategoryId.add(rs.getInt("Child Id"));
				rs.getString("Child");
				rs.getString("Parent");
			}
			isSubFolderExist(listOfCategoryId);
			// get jsonArray by passing the result set
			jsonArrayOfSubFolders = getJsonArrayForFolderWithSubfolder(rs);
		} catch (Exception exception) {
			LOGGER.error(this.getClass().getName() + " --> " + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " --> Error is : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOGGER.info("Exited from getSubModulesForParentFolder() in DynamicQueryTreeHelper.java");
		return jsonArrayOfSubFolders;
	}
	public JSONArray getCustomReports(Integer unitId) {
		LOGGER.info("Entered into getCustomReports() in DynamicQueryTreeHelper.java");
		JSONArray jsonArrayOfCustomReports = new JSONArray();
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			conn = session.connection();
			statement = conn.createStatement();
			rs = statement.executeQuery("select id,reportName,module,submodule from qb_report");

			// get jsonArray by passing the result set
			jsonArrayOfCustomReports = getJsonArrayForReportsWithFolderOrSubfolder(rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("Exited from getCustomReports() in DynamicQueryTreeHelper.java");
		return jsonArrayOfCustomReports;
	}
	private JSONArray getJsonArray(ResultSet rs) {

		LOGGER.info("Entered into getJsonArray() in DynamicQueryTreeHelper.java");
		JSONArray jsonArray = new JSONArray();
		try {
			while (rs.next()) {
				JSONObject jsonObjectOfParentFolder = new JSONObject();
				String module = rs.getString("category_name");
				if (module != null) {
					jsonObjectOfParentFolder.put("id", module);
					jsonObjectOfParentFolder.put("parent", "#");
					jsonObjectOfParentFolder.put("text", module);
					jsonArray.put(jsonObjectOfParentFolder);
				} else {
					jsonObjectOfParentFolder.put("id", "Miscellaneous");
					jsonObjectOfParentFolder.put("parent", "#");
					jsonObjectOfParentFolder.put("text", "Miscellaneous");
					jsonArray.put(jsonObjectOfParentFolder);
				}
			}
		} catch (Exception exception) {
			LOGGER.error(this.getClass().getName() + " --> " + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " --> Error is : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOGGER.info("Exited from getJsonArray() in DynamicQueryTreeHelper.java");
		return jsonArray;
	}

	private JSONArray getJsonArrayForFolderWithSubfolder(ResultSet rs) {

		LOGGER.info("Entered into getJsonArrayForFolderWithSubfolder() in DynamicQueryTreeHelper.java");
		JSONArray jsonArray = new JSONArray();
		try {
			while (rs.next()) {
				JSONObject jsonObjectOfSubFolder = new JSONObject();
				String subFolder = rs.getString("submodule");
				if (subFolder != null) {
					String parentFolder = rs.getString("module");
					jsonObjectOfSubFolder.put("id", subFolder);
					jsonObjectOfSubFolder.put("parent", parentFolder);
					jsonObjectOfSubFolder.put("text", subFolder);
					jsonArray.put(jsonObjectOfSubFolder);
				}
			}
		} catch (Exception exception) {
			LOGGER.error(this.getClass().getName() + " --> " + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " --> Error is : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOGGER.info("Exited from getJsonArrayForFolderWithSubfolder() in DynamicQueryTreeHelper.java");
		return jsonArray;
	}

	private JSONArray getJsonArrayForReportsWithFolderOrSubfolder(ResultSet rs) {
		LOGGER.info("Entered into getJsonArrayForReportsWithFolderOrSubfolder() in DynamicQueryTreeHelper.java");
		JSONArray jsonArray = new JSONArray();
		Integer reportId = null;
		String reportName = null;
		String moduleNameOfReport = null;
		String subModuleNameOfReport = null;
		try {
			while (rs.next()) {
				JSONObject jsonObjectOfCustomReports = new JSONObject();
				reportId = rs.getInt("id");
				reportName = rs.getString("reportName");
				moduleNameOfReport = rs.getString("module");
				subModuleNameOfReport = rs.getString("submodule");
				if (moduleNameOfReport != null && !moduleNameOfReport.equals("")) {
					if (subModuleNameOfReport != null && !subModuleNameOfReport.equals("")) {
						jsonObjectOfCustomReports.put("id", reportId.toString());
						jsonObjectOfCustomReports.put("icon", "jstree-file");
						jsonObjectOfCustomReports.put("parent", subModuleNameOfReport);
						jsonObjectOfCustomReports.put("text", reportName);
						jsonArray.put(jsonObjectOfCustomReports);
					} else {
						jsonObjectOfCustomReports.put("id", reportId.toString());
						jsonObjectOfCustomReports.put("icon", "jstree-file");
						jsonObjectOfCustomReports.put("parent", moduleNameOfReport);
						jsonObjectOfCustomReports.put("text", reportName);
						jsonArray.put(jsonObjectOfCustomReports);
					}
				} else {
					jsonObjectOfCustomReports.put("id", reportId.toString());
					jsonObjectOfCustomReports.put("icon", "jstree-file");
					jsonObjectOfCustomReports.put("parent", "Miscellaneous");
					jsonObjectOfCustomReports.put("text", reportName);
					jsonArray.put(jsonObjectOfCustomReports);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("Exited from getJsonArrayForReportsWithFolderOrSubfolder() in DynamicQueryTreeHelper.java");
		return jsonArray;
	}
	
	public void getReportForNormalEmployee(String userId,String empId,String unitId,String loginPerson)
	{
		LOGGER.info("Entered into getReportForNormalEmployee() in DynamicQueryTreeHelper.java");
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Integer> listOfReportIdCreatedByEmployee=new ArrayList<>();
		try {
			//get list of reportId Created by Employee Himself
			String query="select id from qb_report where qb_report.userid="+userId;
			conn=session.connection();
			statement=conn.createStatement();
			rs=statement.executeQuery(query);
			while(rs.next())
			{
				Integer reportId=rs.getInt("id");
				if(reportId.toString()!=null)
				{
					listOfReportIdCreatedByEmployee.add(rs.getInt(reportId));
				}
			}
			//get Reports that are shared to Employee by hr Admin
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("Exited from getReportForNormalEmployee() in DynamicQueryTreeHelper.java");
	}
	
	private boolean isSubFolderExist(Collection<Integer> listOfCategoryId)
	{
		LOGGER.info("Entered into isSubFolderExist() in DynamicQueryTreeHelper.java");
		boolean subFolderExist=false;
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String x=listOfCategoryId.toString();
		x=x.replace("[", "");
		x=x.replace("]","");
		String query="select id,category_id,category_name from qb_report_category "
										+ "where qb_report_category.category_id in ("+x+")";
		try {
			conn=session.connection();
			statement=conn.createStatement();
			statement.executeQuery(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("Exited from isSubFolderExist() in DynamicQueryTreeHelper.java");
		return subFolderExist;
	}
}

 */
}
