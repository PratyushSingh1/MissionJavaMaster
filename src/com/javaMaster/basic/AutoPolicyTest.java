package com.javaMaster.basic;

public class AutoPolicyTest {

	public static void main(String[] args) {
		
		AutoPolicy policy=new AutoPolicy(1,"Maruti800","Maharastra");
		AutoPolicy policy1=new AutoPolicy(2,"Maruti900","Karnatak");
		policyInNoFaultState(policy);
		policyInNoFaultState(policy1);
	}
	public static void policyInNoFaultState(AutoPolicy autopolicy)
	{
		System.out.println(autopolicy.isNoFaultState() ? "is":"is not");
	}
}
