package com.javaMaster.basic;

//This example is used to calculate Compound Interest
public class Interest {

	public static void main(String[] args) {
		double amount;
		double principal=5000;
		double rate=0.05;
		System.out.println("principal is "+principal+" rate on which it will be calculated is "+rate);
		for(int year=1;year<=10;year++)
		{
			amount=principal*Math.pow(1+rate,year);
			System.out.println("year wise amount "+year+" Amount: "+amount);
		}
	}
}
