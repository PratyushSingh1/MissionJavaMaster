package com.javaMaster.basic;

public class StringCharArray {

	public static void main(String[] args) {
		String s1="hello there";
		char[] charArray=new char[5];
		System.out.println("s1 length"+"  "+s1.length());
		System.out.println("String reversed is");
		for(int counter=s1.length()-1;counter>=0;counter--)
		{
			System.out.println(s1.charAt(counter));
		}
		/**
		 * start and stop position from where the string has to be copied in detination string and the destination postion
		 */
		s1.getChars(0, 5, charArray, 0);
		System.out.println("the character array is: ");
		for(char character:charArray)
		{
			System.out.print(character);
		}
	}

}
