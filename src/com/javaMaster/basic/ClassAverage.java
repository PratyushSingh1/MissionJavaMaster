package com.javaMaster.basic;

import java.util.Scanner;

//This is an example class for average calculation using java
public class ClassAverage {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int total=0;
		int gradeCount=1;
		while(gradeCount<=10)
		{
			System.out.println("Enter the grade :");
			total=total+input.nextInt();
			gradeCount++;
		}
		System.out.println("The Average is :"+total/10);
	}
}
