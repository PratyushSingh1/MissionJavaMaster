package com.javaMaster.basic;

import java.util.Scanner;

public class SwitchStatement {

	public static void main(String[] args) {

		int grades;
		int total=0;
		int gradeCount=0;
		int agrade=0;
		int bgrade=0;
		int cgrade=0;
		int fcount=0;
		int average=0;
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the grades of all students");
		while(input.hasNext())
		{
			grades=input.nextInt();
			total +=grades;
			++gradeCount;
			
			switch (grades/10) {
			case 9:
				++agrade;
				break;

			case 8:
				++bgrade;
				break;
				
			case 7:
				++cgrade;
				break;
				
			default:
				++fcount;
				break;
			}
		}//end of while
		if(gradeCount!=0)
		{
			average=total/gradeCount;
			System.out.println("Total :"+total+" and Average is :"+average);
			System.out.println("Number of Student got A grade :"+agrade);
			System.out.println("Number of Student got B grade :"+bgrade);
			System.out.println("Number of Student got C grade :"+cgrade);
			System.out.println("Number of Student got F grade :"+fcount);
		}
	}
}
