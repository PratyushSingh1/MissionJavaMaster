package com.javaMaster.methods;

import java.util.Scanner;

public class Maximum {
	public static void main(String[] args) {

	Scanner input=new Scanner(System.in);
	System.out.println("Enter three floatingPoints seperated by space :");

	double firstValue=input.nextDouble();
	double secondValue=input.nextDouble();
	double thirdValue=input.nextDouble();
	
	double maximumValue=maximumNumber(firstValue,secondValue,thirdValue);
	System.out.println("Maximum value is :"+maximumValue);
	}

	private static double maximumNumber(double firstValue, double secondValue, double thirdValue) {

		double maximumNumber=firstValue;
		if(firstValue<secondValue)
			maximumNumber=secondValue;
		if(secondValue<thirdValue)
			maximumNumber=thirdValue;
		return maximumNumber;
	}

}
