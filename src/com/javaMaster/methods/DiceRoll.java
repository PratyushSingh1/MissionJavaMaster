package com.javaMaster.methods;

import java.security.SecureRandom;

public class DiceRoll {

	private static int frequency1;
	private static int frequency2;
	private static int frequency3;
	private static int frequency4;
	private static int frequency5;
	private static int frequency6;

	public static void main(String[] args) {

		SecureRandom randomNumber=new SecureRandom();
		for(int counter=0;counter<=60000000;counter++)
		{
			int face=1+randomNumber.nextInt(6);
			switch(face)
			{
			case 1:
				++frequency1;
				break;
			case 2:
				++frequency2;
				break;
			case 3:
				++frequency3;
				break;
			case 4:
				++frequency4;
				break;
			case 5:
				++frequency5;
				break;
			case 6:
				++frequency6;
				break;
			}
		}
		System.out.printf(" frequency of one "+frequency1);
		System.out.println();
		System.out.printf(" frequency of two "+frequency2);
		System.out.println();
		System.out.printf(" frequency of three "+frequency3);
		System.out.println();
		System.out.printf(" frequency of four "+frequency4);
		System.out.println();
		System.out.printf(" frequency of five "+frequency5);
	}

}
