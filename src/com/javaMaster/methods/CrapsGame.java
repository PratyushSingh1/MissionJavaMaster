package com.javaMaster.methods;

import java.security.SecureRandom;

public class CrapsGame {

	private static final  SecureRandom randomNumber=new SecureRandom();
	
	private enum Status{CONTINUE,WON,LOSE};
	
	private static final int TWO=2;
	private static final int THREE=3;
	private static final int SEVEN=7;
	private static final int ELEVEN=11;
	private static final int TWELVE=12;
	
	public static void main(String[] args) {

		int myPoints = 0;
		Status gameStatus;
		
		int sumOfDiceRoll=rollDice();
		switch(sumOfDiceRoll)
		{
		case SEVEN:
		case ELEVEN:
			gameStatus=Status.WON;
			break;
			
		case TWO:
		case THREE:
		case TWELVE:
			gameStatus=Status.LOSE;
			break;
		default:
			gameStatus=Status.CONTINUE;
			myPoints=sumOfDiceRoll;
			System.out.println("My Points Are: "+myPoints);
		}
		while(gameStatus==Status.CONTINUE)
		{
			sumOfDiceRoll=rollDice();
			if(sumOfDiceRoll==myPoints)
			{
				gameStatus=Status.WON;
			}
			else if(sumOfDiceRoll==SEVEN)
			{
				gameStatus=Status.LOSE;
			}
		}
		if(gameStatus==Status.WON)
		{
			System.out.println("We Won");
		}
		else
			System.out.println("We Lose");
	}

	private static int rollDice() {

		int firstDiceRoll=randomNumber.nextInt(6);
		int secondDiceRoll=randomNumber.nextInt(6);
		int sumOfDice=firstDiceRoll+secondDiceRoll;
		System.out.println("First Dice Roll : "+firstDiceRoll+"Second Dice Roll :+ "+secondDiceRoll+" sum "+sumOfDice);
		return sumOfDice;
	}

}
