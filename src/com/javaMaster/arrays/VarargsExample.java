package com.javaMaster.arrays;

public class VarargsExample {

	public static void main(String[] args) {

		double d1=10.0;
		double d2=20.0;
		double d3=30.0;
		double d4=40.0;
		
		System.out.println("Average of d1 and d2 "+average(d1,d2));
		System.out.println("Average of d1 and d2 and d3 "+average(d1,d2,d3));
		System.out.println("Average of d1 and d2 and d3 and d4 "+average(d1,d2,d3,d4));
	}

	private static double average(double... numbers)
	{
		int total=0;
		for(double marks:numbers)
		{
			total+=marks;
		}
		return total/numbers.length;
	}
}
