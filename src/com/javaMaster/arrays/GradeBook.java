package com.javaMaster.arrays;

public class GradeBook {

	private String courseName;
	private int[] grades;
	public GradeBook(String string) {

		this.courseName=string;
	}

	
	public String getCourseName() {
		return courseName;
	}


	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}


	public int[] getGrades() {
		return grades;
	}


	public void setGrades(int[] grades) {
		this.grades = grades;
	}


	public void processGrades() {
		outputGrades();
		getAverageOfGrades();
		getMinimum();getMaximum();
	}

	private int getMaximum() {
		int max=grades[0];
		for(int grade:grades)
		{
			if(grade>max)
				max=grade;
		}
		return max;
	}


	private int getMinimum() {
		int min=grades[0];
		for(int grade:grades)
		{
			if(grade<min)
				min=grade;
		}
		return min;
	}


	private double getAverageOfGrades() {
		int total=0;
		for(int grade:grades)
		{
			total+=grade;
		}
		double average=(double)total/grades.length;
		System.out.println("Average of the Class With total "+total+"  is"+average);
		return average;
	}


	private void outputGrades() {

		for(int grade:grades)
		{
			System.out.println("Student and his grade is-- "+grade);
		}
	}

	

}
