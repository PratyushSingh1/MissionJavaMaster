package com.javaMaster.arrays;

import java.security.SecureRandom;

public class DeckOfCards {

	private Cards[] deck;
	private int current_cards;
	private static final int NUMBER_OF_CARDS=52;
	private static final SecureRandom randomNumbers=new SecureRandom();
	
	public DeckOfCards()
	{
		String[] faces={"Ace","Deuce","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"};
		String[] suits={"Heart","Diamond","Clubs","Spades"};
		
		deck=new Cards[NUMBER_OF_CARDS];
		current_cards=0;
		for(int counter=0;counter<deck.length;counter++)
		{
			deck[counter]=new Cards(faces[counter%13], suits[counter/13]);
		}
	}//end of Constructor Initialization
	
	public void shuffleTheCards()
	{
		current_cards=0;
		for(int first=0;first<deck.length;first++)
		{
			int second=randomNumbers.nextInt(NUMBER_OF_CARDS);
			Cards temp=deck[first];
			deck[first]=deck[second];
			deck[second]=temp;
		}
	}//end of shuffle method
	
	public Cards dealCards()
	{
		if(current_cards<deck.length)
			return deck[current_cards++];
		else
			return null;
	}
}
