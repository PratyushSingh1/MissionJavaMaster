package com.javaMaster.arrays;

public class ArraysBasic {

	public static void main(String[] args) {

		final int ARRAY_LENGTH=10;
		int[] array=new int[ARRAY_LENGTH];
		System.out.println("First Load An Array");
		for(int counter=0;counter<array.length;counter++)
		{
			array[counter]=10+counter;
		}
		System.out.println("Now Print An Array");
		for(int counter=0;counter<=array.length;counter++)
		{
			System.out.println(array[counter]);
		}
	}

}
