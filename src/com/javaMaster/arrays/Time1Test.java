package com.javaMaster.arrays;

public class Time1Test {

	public static void main(String[] args) {

		Time1 time=new Time1();
		displayTime("After the Time Object Has been created ",time);
		System.out.println();
		
		time.setTime(13, 23, 00);
		displayTime("After the Time Has Been set", time);
	}
private static void displayTime(String header, Time1 time)
{
	System.out.printf("%s%n UniversalTime:%s%n Standard Time: %s%n",header,time.toUniversalString(),time.toString());
}
}
