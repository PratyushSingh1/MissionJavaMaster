package com.javaMaster.arrays;

import java.security.SecureRandom;

public class ArraysDiceRoll {

	public static void main(String[] args) {

		SecureRandom random=new SecureRandom();
		int[] frequency=new int[7];
		
		for(int roll=1;roll<=6000000;roll++)
		{
			//formula is f(randomNumber)=f(randomNumber)+1
			//here we are adding 1 to randomNumber generator so that 0 can be excluded since random numbers are 
			//generated from 0 but dice doesn't has o in it!! 
			int x=1+random.nextInt(6);
			frequency[x]=frequency[x]+1;
			//we can use the below line of code instead to make it short 
			//++frequency[1+random.nextInt(6)];
		}
		for(int face=1;face<frequency.length;face++)
		{
			System.out.println(face+"  "+frequency[face]);
		}
	}

}
