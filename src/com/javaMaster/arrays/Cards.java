package com.javaMaster.arrays;

public class Cards {

	private final String faces;
	private final String suits;
	
	public Cards(String faces, String suits) {
		super();
		this.faces = faces;
		this.suits = suits;
	}

	@Override
	public String toString() {
		return faces+" of "+suits;
	}
}
