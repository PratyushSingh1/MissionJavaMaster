package com.javaMaster.arrays;

public class GradeBookTest {

	public static void main(String[] args) {

		int[] gradeArray={87,68,64,65,77,87,99,68,88};
		GradeBook myGradeBook=new GradeBook("Java Programing Course");
		System.out.println("Welcome to grade book class");
		System.out.println(myGradeBook.getCourseName());
		myGradeBook.processGrades();
	}

}
