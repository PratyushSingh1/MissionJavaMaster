package com.javaMaster.arrays;

import java.sql.Array;
import java.util.Arrays;

public class ArraysClassExample {

	public static void main(String[] args) {

		int[] array={2,2,5,7,1,7,4,9,3};
		Arrays.sort(array);
		for(int number:array)
			System.out.println("Sorted Array Is "+number);
		System.out.println("position of 5 in the array is "+Arrays.binarySearch(array, 5));
	}
}
