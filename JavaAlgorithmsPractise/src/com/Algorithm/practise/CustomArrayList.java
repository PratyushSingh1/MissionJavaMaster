package com.Algorithm.practise;

public class CustomArrayList {

	private int SIZE_FACTOR=5;
	private Object[] data;
	private int index;
	private int size;
	
	public CustomArrayList() {
		data=new Object[SIZE_FACTOR];
		size=SIZE_FACTOR;
	}
	
	public void add(Object element) {
		if(index==size-1) {
			size=size+SIZE_FACTOR;
			Object[] newData=new Object[size];
			for(int i=0;i<data.length;i++) {
				newData[i]=data[i];
			}
			data=newData;
		}
		data[index]=element;
		index++;
	}
	
	public Object get(int dataPosition) {
		Object returnObject=data[dataPosition];
		return returnObject;
	}
	
	public void remove(int dataPosition) {
		for(int x=dataPosition;x<data.length-1;x++) {
			data[x]=data[x+1];
		}
		this.index--;
	}
	
	public static void main(String[] args) {
		CustomArrayList arrayList=new CustomArrayList();
		arrayList.add("pratyush");
		arrayList.add("Sudheer");
		arrayList.add("Ranjan");
		arrayList.add("Monika");
		arrayList.add("Siva");
		arrayList.add("Dhananjay");
		arrayList.add("Ashok");
		System.out.println(arrayList.get(0));
		System.out.println(arrayList.get(1));
		System.out.println(arrayList.get(2));
		System.out.println(arrayList.get(3));
		System.out.println(arrayList.get(4));
		System.out.println(arrayList.get(5));
		System.out.println(arrayList.get(6));
	}
	
}
