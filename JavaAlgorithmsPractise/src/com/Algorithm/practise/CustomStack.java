package com.Algorithm.practise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomStack<T> {

	private int INITIAL_SIZE=10;
	private Object[] elements;
	private int currentElementPosition=0;
	
	public CustomStack() {
		super();
		elements=new Object[INITIAL_SIZE];
	}
	
	public void push(T elementToPush) {
		if(currentElementPosition==elements.length) {
			int newSize=elements.length*2;
			elements=Arrays.copyOf(elements, newSize);
		}
		elements[currentElementPosition++]=elementToPush;
	}
	
	public T pop() {
			T returnObject=null;
			try {
				returnObject = (T) elements[--currentElementPosition];
				elements[currentElementPosition]=null;
			} catch (Exception e) {
				throw new RuntimeException("Can't pop empty stack");
			}
			return returnObject;
	}
	
	public T peek() {
		T returnObject=(T) elements[currentElementPosition--];
		return returnObject;
	}
	
	public Iterable<T> getFullIterableStack(){
		List<Object> iterableToReturn=new ArrayList<Object>();
		for(int i=0;i<elements.length;i++) {
			iterableToReturn.add(elements[i]);
		}
		return (Iterable<T>) iterableToReturn;
	}
	
	public static void main(String[] args) {
		CustomStack<String> customStack=new CustomStack<String>();
		customStack.push("pratyush");
		customStack.push("Ram");
		customStack.push("Sita");
		customStack.push("Radha");
		customStack.push("Shiva");
		customStack.push("Parmeshwar");
		customStack.push("Nisha");
		customStack.push("Jitender");
		customStack.push("Lakshman");
		customStack.push("Rohit");
		customStack.push("Mohit");
		Iterable<String> iterable=customStack.getFullIterableStack();
		for(String name:iterable) {
			System.out.println(name);
		}
	}
}
